%  Relevant files

InitCondFile  		./ics
OutputDir               ./

EnergyFile        energy.txt
InfoFile          info.txt
TimingsFile       timings.txt
CpuFile           cpu.txt

SnapshotFileBase  snap

RestartFile       restart



% CPU-time limit

TimeLimitCPU      129600
ResubmitOn        0
ResubmitCommand   /home/vspringe/autosubmit


% Code options

ICFormat              1
SnapFormat            2

ComovingIntegrationOn 1

NumFilesPerSnapshot       8
NumFilesWrittenInParallel 8

CoolingOn       1
StarformationOn 1
 
%  Caracteristics of run

TimeBegin           0.01    % z=0
TimeMax	            0.25    % end at z=0

Omega0	              0.3175   % total matter density
OmegaLambda           0.6825
OmegaBaryon           0.0490     ; maybe there are no baryons
HubbleParam           0.6711     ; only needed for cooling

BoxSize                15000.0
PeriodicBoundariesOn   1


% Softening lengths

MinGasHsmlFractional     0.1  % minimum gas smoothing in terms of the gravitational softening length


SofteningGas       3.0
SofteningHalo      3.0   
SofteningDisk      3.0
SofteningBulge     3.0
SofteningStars     3.0
SofteningBndry     3.0

SofteningGasMaxPhys       3.0
SofteningHaloMaxPhys      3.0
SofteningDiskMaxPhys      3.0
SofteningBulgeMaxPhys     3.0
SofteningStarsMaxPhys     3.0
SofteningBndryMaxPhys     3.0



% Output frequency

OutputListOn        1
OutputListFilename   ./times.txt

TimeBetSnapshot        1.
TimeOfFirstSnapshot    1.
 
CpuTimeBetRestartFile  21600.0  

TimeBetStatistics      0.25

MaxRMSDisplacementFac  0.2

% Accuracy of time integration

TypeOfTimestepCriterion 0   
	                    
ErrTolIntAccuracy       0.025  


MaxSizeTimestep        0.025
MinSizeTimestep        0.0


% Tree algorithm and force accuracy

ErrTolTheta             0.5

TypeOfOpeningCriterion  1
ErrTolForceAcc          0.005

DesLinkNgb              20
ErrTolThetaSubfind      0.45


TreeDomainUpdateFrequency    0.01


%  Parameters of SPH

DesNumNgb           40
MaxNumNgbDeviation  2

ArtBulkViscConst    1.0

InitGasTemp         272.8  % initial gas temperature in K, only used if not given in IC file

MinGasTemp          25.0    
CourantFac          0.15


% Further code parameters

PartAllocFactor       1.5  
%TreeAllocFactor       0.75
MaxMemSize	       1900

BufferSize              100



% System of units

UnitLength_in_cm         3.085678e21        ;  1.0 kpc
UnitMass_in_g            1.989e43           ;  1.0e10 solar masses
UnitVelocity_in_cm_per_s 1e5                ;  1 km/sec
GravityConstantInternal  0







CritPhysDensity                    0
MaxSfrTimescale                    1.5
CritOverDensity                    1000.0
TempSupernova                      1e+08
TempClouds                         1000
FactorSN                           0.1
FactorEVP                          1000
WindEfficiency                     2
WindFreeTravelLength               20
WindEnergyFraction                 1
WindFreeTravelDensFac              0.1

%TimeFirstLineOfSight               1.0






