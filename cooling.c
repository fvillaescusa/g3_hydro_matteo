#ifndef BG_COOLING

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "allvars.h"
#include "proto.h"

#define EXC_SW91

#include "cooling.h"

#ifdef SFR_METALS
#include "c_metals.h"
#endif

#ifdef COOLING

#define NCOOLTAB  2000

#define SMALLNUM 1.0e-60
#define COOLLIM  0.1
#define HEATLIM	 20.0

#ifndef SFR_METALS
static double XH = HYDROGEN_MASSFRAC;	/* hydrogen abundance by mass */
static double yhelium;
#endif

#define eV_to_K   11606.0
#define eV_to_erg 1.60184e-12


static double mhboltz;		/* hydrogen mass over Boltzmann constant */
static double ethmin;		/* minimum internal energy for neutral gas */

static double Tmin = 0.0;	/* in log10 */
static double Tmax = 9.0;
static double deltaT;

static double *BetaH0, *BetaHe0, *BetaHep, *Betaff;
static double *AlphaHp, *AlphaHep, *Alphad, *AlphaHepp;
static double *GammaeH0, *GammaeHe0, *GammaeHep;

static double J_UV = 0, gJH0 = 0, gJHep = 0, gJHe0 = 0, epsH0 = 0, epsHep = 0, epsHe0 = 0;

static double ne, necgs, nHcgs;
static double bH0, bHe0, bHep, bff, aHp, aHep, aHepp, ad, geH0, geHe0, geHep;
static double gJH0ne, gJHe0ne, gJHepne;
static double nH0, nHp, nHep, nHe0, nHepp;

static double overden;

int rescale_flag=0;

#ifdef LT_METAL_COOLING
double GetMetalLambda(double, double);
#endif

/* HeII heating rate modifications */
#define ZSCALE 9.0  /* Redshift below which rescaling happens */
#define AMPL 1.0    /* Factor by which heating rates are multplied */
#define GRAD -0.40   /* Density dependent heating gradient, E*=Delta^Grad */
#define DTHRESH 10.0/* Density threshold for power law flattening */

/******************************************************************************/
/* returns new internal energy per unit mass. 
 * Arguments are passed in code units, density is proper density.
 */
#ifdef LT_METAL_COOLING
double DoCooling(double u_old, double rho, double dt, double *ne_guess, double Z)
#else
double DoCooling(double u_old, double rho, double dt, double *ne_guess)
#endif
{
  
  double u, du, rhoc, redshift;
  double nH0_input,nHep_input,ne_input;
  double u_lower, u_upper, u_min, molec;
  double ratefact, t_count, LambdaNet;
  double u_input, dmax1, dmax2;
  int iter = 0;
 

  /* convert to physical cgs units */
  rho *= All.UnitDensity_in_cgs * All.HubbleParam * All.HubbleParam; 
  u_old *= All.UnitPressure_in_cgs / All.UnitDensity_in_cgs;
  dt *= All.UnitTime_in_s / All.HubbleParam;
  
  /* Compute overdensity */
  redshift = 1 / All.Time - 1;
  rhoc = 3.0 * 0.72*HUBBLE * 0.72*HUBBLE /(8.0*M_PI*GRAVITY);
  overden = rho/(0.0444 * rhoc * 
		 (1.+redshift)*(1.+redshift)*(1.+redshift));
  
  if(redshift  < ZSCALE)
    rescale_flag = 1;
  else
    rescale_flag = 0;
  

  ne_input = *ne_guess;
  u_input = u_old;
  
  molec =  (1.0 + 4.0 * yhelium) / (1.0 + yhelium + ne_input);
  u_min = (BOLTZMANN * All.MinGasTemp)/(GAMMA_MINUS1 * molec * PROTONMASS);
  
  u_old = DMAX(u_min, u_old);
    
  nHcgs = XH * rho / PROTONMASS;	/* hydrogen number dens in cgs units */
  ratefact = nHcgs * nHcgs / rho;
  
  u = u_old;
  u_lower = u;
  u_upper = u;
  
#ifdef LT_METAL_COOLING
  LambdaNet = CoolingRateFromU(u, rho, ne_guess, Z);
#else
  LambdaNet = CoolingRateFromU(u, rho, ne_guess);
#endif
  
  /* bracketing */
  
  if(u - u_old - ratefact * LambdaNet * dt < 0)	/* heating */
    {
      u_upper *= sqrt(1.1);
      u_lower /= sqrt(1.1);
#ifdef LT_METAL_COOLING
      while(u_upper - u_old - ratefact * CoolingRateFromU(u_upper, rho, ne_guess, Z) * dt < 0)
#else
	while(u_upper - u_old - ratefact * CoolingRateFromU(u_upper, rho, ne_guess) * dt < 0)
#endif
	  {
	    u_upper *= 1.1;
	    u_lower *= 1.1;
	  }
      
    }
  
  if(u - u_old - ratefact * LambdaNet * dt > 0)
    {
      u_lower /= sqrt(1.1);
      u_upper *= sqrt(1.1);
#ifdef LT_METAL_COOLING
      while(u_lower - u_old - ratefact * CoolingRateFromU(u_lower, rho, ne_guess, Z) * dt > 0)
#else
	while(u_lower - u_old - ratefact * CoolingRateFromU(u_lower, rho, ne_guess) * dt > 0)
#endif
	  {
	    u_upper /= 1.1;
	    u_lower /= 1.1;
	  }
    }
  
  do
    {
      u = 0.5 * (u_lower + u_upper);
      
#ifdef LT_METAL_COOLING
      LambdaNet = CoolingRateFromU(u, rho, ne_guess, Z);
#else
      LambdaNet = CoolingRateFromU(u, rho, ne_guess);
#endif
      
      if(u - u_old - ratefact * LambdaNet * dt > 0)
	{
	  u_upper = u;
	}
      else
	{
	  u_lower = u;
	}
      
      du = u_upper - u_lower;
      
      iter++;
	  
      if(iter >= (MAXITER - 10))
	printf("u= %g\n", u);
    }
  while(fabs(du / u) > 1.0e-6 && iter < MAXITER);
  
  if(iter >= MAXITER)
    {
      printf("Equilibrium energy convergence not reached\n");
      endrun(10);
    }

  
  u *= All.UnitDensity_in_cgs / All.UnitPressure_in_cgs;	/* to internal units */
  
  return u;
}

/***************************************************************************************************/
/* returns cooling time. 
 * NOTE: If we actually have heating, a cooling time of 0 is returned.
 */
#ifdef LT_METAL_COOLING
double GetCoolingTime(double u_old, double rho, double *ne_guess, double Z)
#else 
double GetCoolingTime(double u_old, double rho, double *ne_guess)
#endif
{
  double u;
  double ratefact;
  double LambdaNet, coolingtime;
  
  
  rho *= All.UnitDensity_in_cgs * All.HubbleParam * All.HubbleParam;	/* convert to physical cgs units */
  u_old *= All.UnitPressure_in_cgs / All.UnitDensity_in_cgs;


  nHcgs = XH * rho / PROTONMASS;	/* hydrogen number dens in cgs units */
  ratefact = nHcgs * nHcgs / rho;

  u = u_old;

#ifdef LT_METAL_COOLING
  LambdaNet = CoolingRateFromU(u, rho, ne_guess, Z);
#else
  LambdaNet = CoolingRateFromU(u, rho, ne_guess);
#endif

  /* bracketing */

  if(LambdaNet >= 0)		/* we have actually heating due to UV background */
    return 0;

  coolingtime = u_old / (-ratefact * LambdaNet);

  coolingtime *= All.HubbleParam / All.UnitTime_in_s;

  return coolingtime;
}

/******************************************************************************/

/*  this function first computes the self-consistent temperature
 *  and abundance ratios, and then it calculates 
 *  (heating rate-cooling rate)/n_h^2 in cgs units 
 */
#ifdef LT_METAL_COOLING
double CoolingRateFromU(double u, double rho, double *ne_guess, double Z)
#else
double CoolingRateFromU(double u, double rho, double *ne_guess)
#endif
{
  double temp;

  temp = convert_u_to_temp(u, rho, ne_guess);

#ifdef LT_METAL_COOLING
  return CoolingRate(log10(temp), rho, ne_guess, Z);
#else
#ifndef SFR_METALS
  return CoolingRate(log10(temp), rho, ne_guess);
#else
  return CoolingRate_SD(log10(temp), rho, ne_guess);
#endif
#endif
}
/******************************************************************************/
/* this function determines the electron fraction, and hence the mean 
 * molecular weight. With it arrives at a self-consistent temperature.
 * Element abundances and the rates for the emission are also computed
 */
double convert_u_to_temp(double u, double rho, double *ne_guess)
{
  double temp, temp_old, temp_new, max = 0, ne_old;
  double mu, dmax1, dmax2;
  int iter = 0;

  double u_input, rho_input, ne_input;

  yhelium = (1.0 - XH) / (4.0 * XH);
  
  u_input = u;
  rho_input = rho;
  

  ne_input = *ne_guess;
  
  
  mu = (1 + 4 * yhelium) / (1 + yhelium + ne_input);
  temp = GAMMA_MINUS1 / BOLTZMANN * u * PROTONMASS * mu;
  
  
  do
    {
      
      ne_old = *ne_guess;
      
      find_abundances_and_rates(log10(temp), rho, ne_guess);
      temp_old = temp;
      
      ne_input = *ne_guess;
      
      mu = (1 + 4 * yhelium) / (1 + yhelium + ne_input );
      
      temp_new = GAMMA_MINUS1 / BOLTZMANN * u * PROTONMASS * mu;
      
      max =
	DMAX(max,
	     temp_new / (1 + yhelium + ne_input ) * fabs(( ne_input - ne_old) 
							 / (temp_new - temp_old + 1.0)));
      
      temp = temp_old + (temp_new - temp_old) / (1 + max);
      iter++;
      
      if(iter > (MAXITER - 10))
	printf("-> temp= %g ne=%g\n", temp, *ne_guess);
    }
  while(fabs(temp - temp_old) > 1.0e-3 * temp && iter < MAXITER);

  
  if(iter >= MAXITER)
    {
      printf("failed to converge in convert_u_to_temp()\n");
      printf("u_input= %g\nrho_input=%g\n ne_input=%g\n", u_input, rho_input, ne_input);
      endrun(12);
    }
  
  return temp;
}
/******************************************************************************/

/* this function computes the actual abundance ratios 
 */
void find_abundances_and_rates(double logT, double rho, double *ne_guess)
{
  double neold, nenew;
  int j, niter;
  double Tlow, Thi, flow, fhi, t;
  double logT_input, rho_input;
  
  yhelium = (1.0 - XH) / (4.0 * XH);
  
  
  logT_input = logT;
  rho_input = rho;
  
  
  ne = *ne_guess;
  
  
  if(logT <= Tmin)		/* everything neutral */
    {
      nH0 = 1.0;
      nHe0 = yhelium;
      nHp = 0;
      nHep = 0;
      nHepp = 0;
      ne = 0;
      *ne_guess = ne;
      return;
    }

  if(logT >= Tmax)		/* everything is ionized */
    {
      nH0 = 0;
      nHe0 = 0;
      nHp = 1.0;
      nHep = 0;
      nHepp = yhelium;
      ne = nHp + 2.0 * nHepp;
      *ne_guess = ne;
      return;
    }
  
  
  t = (logT - Tmin) / deltaT;
  j = (int) t;
  Tlow = Tmin + deltaT * j;
  Thi = Tlow + deltaT;
  fhi = t - j;
  flow = 1 - fhi;
  
  if(ne == 0)
    ne = 1.0;
  
  nHcgs = XH * rho / PROTONMASS;	/* hydrogen number dens in cgs units */
  
  neold = ne;
  niter = 0;
  necgs = ne * nHcgs;
  
  /* evaluate number densities iteratively (cf KWH eqns 33-38) in units of nH */
  do
    {
      niter++;
      
      aHp = flow * AlphaHp[j] + fhi * AlphaHp[j + 1];
      aHep = flow * AlphaHep[j] + fhi * AlphaHep[j + 1];
      aHepp = flow * AlphaHepp[j] + fhi * AlphaHepp[j + 1];
      ad = flow * Alphad[j] + fhi * Alphad[j + 1];
      geH0 = flow * GammaeH0[j] + fhi * GammaeH0[j + 1];
      geHe0 = flow * GammaeHe0[j] + fhi * GammaeHe0[j + 1];
      geHep = flow * GammaeHep[j] + fhi * GammaeHep[j + 1];

      if(necgs <= 1.e-25 || J_UV == 0)
	{
	  gJH0ne = gJHe0ne = gJHepne = 0;
	}
      else
	{
	  gJH0ne = gJH0 / necgs;
	  gJHe0ne = gJHe0 / necgs;
	  gJHepne = gJHep / necgs;
	}

      nH0 = aHp / (aHp + geH0 + gJH0ne);	/* eqn (33) */
      nHp = 1.0 - nH0;		/* eqn (34) */

      if((gJHe0ne + geHe0) <= SMALLNUM)	/* no ionization at all */
	{
	  nHep = 0.0;
	  nHepp = 0.0;
	  nHe0 = yhelium;
	}
      else
	{
	  nHep = yhelium / (1.0 + (aHep + ad) / (geHe0 + gJHe0ne) + (geHep + gJHepne) / aHepp);	/* eqn (35) */
	  nHe0 = nHep * (aHep + ad) / (geHe0 + gJHe0ne);	/* eqn (36) */
	  nHepp = nHep * (geHep + gJHepne) / aHepp;	/* eqn (37) */
	}

      neold = ne;

      ne = nHp + nHep + 2 * nHepp;	/* eqn (38) */
      necgs = ne * nHcgs;

      if(J_UV == 0)
	break;

      nenew = 0.5 * (ne + neold);
      ne = nenew;
      necgs = ne * nHcgs;

      if(fabs(ne - neold) < 1.0e-4)
	break;

      if(niter > (MAXITER - 10))
	printf("ne= %g  niter=%d\n", ne, niter);
    }
  while(niter < MAXITER);

  if(niter >= MAXITER)
    {
      printf("Equilibrium abundance convergence not reached\n");
      printf("nH0 = %e\n",nH0);
      printf("nHp = %e\n",nHp);
      printf("nHe0 = %e\n",nHe0);  
      printf("nHep = %e\n",nHep); 
      printf("nHepp = %e\n",nHepp);
      printf("ne = %e\n",ne);
      endrun(13);
    }

  bH0 = flow * BetaH0[j] + fhi * BetaH0[j + 1];
  bHe0 = flow * BetaHe0[j] + fhi * BetaHe0[j + 1];
  bHep = flow * BetaHep[j] + fhi * BetaHep[j + 1];
  bff = flow * Betaff[j] + fhi * Betaff[j + 1];

  *ne_guess = ne;
}

/******************************************************************************/
/*  this function computes the self-consistent temperature
 *  and abundance ratios 
 */
double AbundanceRatios(double u, double rho, double *ne_guess, double *nH0_pointer, double *nHeII_pointer)
{
  double temp;

  rho *= All.UnitDensity_in_cgs * All.HubbleParam * All.HubbleParam;	/* convert to physical cgs units */
  u *= All.UnitPressure_in_cgs / All.UnitDensity_in_cgs;

  temp = convert_u_to_temp(u, rho, ne_guess);
  
  *nH0_pointer = nH0;
  *nHeII_pointer = nHep;

  return temp;
}
/******************************************************************************/
extern FILE *fd;

/*  Calculates (heating rate-cooling rate)/n_h^2 in cgs units 
 */
#ifdef LT_METAL_COOLING
double CoolingRate(double logT, double rho, double *nelec, double Z)
#else
double CoolingRate(double logT, double rho, double *ne_guess)
#endif
{
  double Lambda, Heat;
  double LambdaExc, LambdaIon, LambdaRec, LambdaFF, LambdaCmptn = 0.0;
  double LambdaExcH0, LambdaExcHep, LambdaIonH0, LambdaIonHe0, LambdaIonHep;
  double LambdaRecHp, LambdaRecHep, LambdaRecHepp, LambdaRecHepd, LambdaExcHe0;
  double redshift;
  double T;

  /* RESCALE CODE */
  double epsH0_old,epsHe0_old,epsHep_old;
  
  epsH0_old  = epsH0;
  epsHe0_old = epsHe0;
  epsHep_old = epsHep; 
  redshift = 1/All.Time - 1;
  
  if (rescale_flag == 1)
    {
      if (overden <= DTHRESH)
	{
	  epsH0  *= pow(overden,GRAD);
	  epsHe0 *= pow(overden,GRAD);
	  epsHep *= pow(overden,GRAD);
	}
      else
	{
	  epsH0  *= pow(DTHRESH,GRAD);
	  epsHe0 *= pow(DTHRESH,GRAD);
	  epsHep *= pow(DTHRESH,GRAD);
	}
    }
  /* RESCALE CODE */
  


  yhelium = (1.0 - XH) / (4 * XH);

  if(logT <= Tmin)
    logT = Tmin + 0.5 * deltaT;	/* floor at Tmin */


  nHcgs = XH * rho / PROTONMASS;	/* hydrogen number dens in cgs units */
  
  
  if(logT < Tmax)
    {
      find_abundances_and_rates(logT, rho, ne_guess);
      
      /* Compute cooling and heating rate (cf KWH Table 1) in units of nH**2 */
#ifdef LT_METAL_COOLING
      /* get cooling rate from sutherland and dopita tables */
      /* keeping abundances and rates calculations */
      
      if((Z >= ZMin) && (logT >= TMin))
	Lambda = GetMetalLambda(logT, Z) * ne * (nHp + nHep + nHepp);
      else
	{
#endif
	  T = pow(10.0, logT);

	  LambdaExcH0 = bH0 * ne * nH0;
	  LambdaExcHe0 = bHe0 * ne * ne * nHe0;
	  LambdaExcHep = bHep * ne * nHep;
	  LambdaExc = LambdaExcH0 + LambdaExcHe0 + LambdaExcHep;	/* excitation */

	  LambdaIonH0 = 2.18e-11 * geH0 * ne * nH0;
	  LambdaIonHe0 = 3.94e-11 * geHe0 * ne * nHe0;
	  LambdaIonHep = 8.72e-11 * geHep * ne * nHep;
	  LambdaIon = LambdaIonH0 + LambdaIonHe0 + LambdaIonHep;	/* ionization */

	  LambdaRecHp = 1.036e-16 * T * ne * (aHp * nHp);
	  LambdaRecHep = 1.036e-16 * T * ne * (aHep * nHep);
	  LambdaRecHepp = 1.036e-16 * T * ne * (aHepp * nHepp);
	  LambdaRecHepd = 6.526e-11 * ad * ne * nHep;
	  LambdaRec = LambdaRecHp + LambdaRecHep + LambdaRecHepp + LambdaRecHepd;

	  LambdaFF = bff * (nHp + nHep + 4 * nHepp) * ne;

	  Lambda = LambdaExc + LambdaIon + LambdaRec + LambdaFF;

	  if(All.ComovingIntegrationOn)
	    {
	      redshift = 1 / All.Time - 1;
	      LambdaCmptn = 5.65e-36 * ne * (T - 2.73 * (1. + redshift)) * pow(1. + redshift, 4.) / nHcgs;

	      Lambda += LambdaCmptn;
	    }
	  else
	    LambdaCmptn = 0;
#ifdef LT_METAL_COOLING
	}
#endif

      Heat = 0;
      if(J_UV != 0)
	Heat += (nH0 * epsH0 + nHe0 * epsHe0 + nHep * epsHep) / nHcgs;
    }
  else				/* here we're outside of tabulated rates, T>Tmax K */
    {
      /* at high T (fully ionized); only free-free and Compton cooling are present.  
         Assumes no heating. */

      Heat = 0;

      LambdaExcH0 = LambdaExcHep = LambdaIonH0 = LambdaIonHe0 = LambdaIonHep =
	LambdaRecHp = LambdaRecHep = LambdaRecHepp = LambdaRecHepd = 0;

      /* very hot: H and He both fully ionized */
      nHp = 1.0;
      nHep = 0;
      nHepp = yhelium;
      ne = nHp + 2.0 * nHepp;
      *ne_guess = ne;		/* note: in units of the hydrogen number density */
      
      T = pow(10.0, logT);
      LambdaFF =
	1.42e-27 * sqrt(T) * (1.1 + 0.34 * exp(-(5.5 - logT) * (5.5 - logT) / 3)) * (nHp + 4 * nHepp) * ne;
      
      if(All.ComovingIntegrationOn)
	{
	  redshift = 1 / All.Time - 1;
	  /* add inverse Compton cooling off the microwave background */
	  LambdaCmptn = 5.65e-36 * ne * (T - 2.73 * (1. + redshift)) * pow(1. + redshift, 4.) / nHcgs;
	}
      else
	LambdaCmptn = 0;

      Lambda = LambdaFF + LambdaCmptn;
    }
  
  /* RESCALE CODE */
  epsH0  = epsH0_old;
  epsHe0 = epsHe0_old;
  epsHep = epsHep_old;
  /* RESCALE CODE */

  return (Heat - Lambda);
}
/******************************************************************************/
void InitCoolMemory(void)
{
  BetaH0 = (double *) mymalloc("BetaH0", (NCOOLTAB + 1) * sizeof(double));
  BetaHe0 = (double *) mymalloc("BetaHe0", (NCOOLTAB + 1) * sizeof(double));
  BetaHep = (double *) mymalloc("BetaHep", (NCOOLTAB + 1) * sizeof(double));
  AlphaHp = (double *) mymalloc("AlphaHp", (NCOOLTAB + 1) * sizeof(double));
  AlphaHep = (double *) mymalloc("AlphaHep", (NCOOLTAB + 1) * sizeof(double));
  Alphad = (double *) mymalloc("Alphad", (NCOOLTAB + 1) * sizeof(double));
  AlphaHepp = (double *) mymalloc("AlphaHepp", (NCOOLTAB + 1) * sizeof(double));
  GammaeH0 = (double *) mymalloc("GammaeH0", (NCOOLTAB + 1) * sizeof(double));
  GammaeHe0 = (double *) mymalloc("GammaeHe0", (NCOOLTAB + 1) * sizeof(double));
  GammaeHep = (double *) mymalloc("GammaeHep", (NCOOLTAB + 1) * sizeof(double));
  Betaff = (double *) mymalloc("Betaff", (NCOOLTAB + 1) * sizeof(double));
}
/******************************************************************************/
/*  Set up interpolation tables in T for cooling rates. */
void MakeCoolingTable(void)  
{
  int i;
  double T, T_eV;
  double Tfact;

  XH = 0.76;
  yhelium = (1.0 - XH) / (4 * XH);

  
  mhboltz = PROTONMASS / BOLTZMANN;


  if(All.MinGasTemp > 0.0)
    Tmin = log10(0.1 * All.MinGasTemp);
  else
    Tmin = 1.0;


  deltaT = (Tmax - Tmin) / NCOOLTAB;


  /* minimum internal energy for neutral gas */
  ethmin = pow(10.0, Tmin) * (1. + yhelium) / ((1. + 4. * yhelium) * mhboltz * GAMMA_MINUS1);
  

  for(i = 0; i <= NCOOLTAB; i++)
    {


      BetaH0[i] = BetaHe0[i] = BetaHep[i] = Betaff[i] = AlphaHp[i] = 
	AlphaHep[i] = AlphaHepp[i] = Alphad[i] = GammaeH0[i] = GammaeHe0[i] = GammaeHep[i] = 0;

      T = pow(10.0, Tmin + deltaT * i);

      /* Temperature in eV */
      T_eV =  T * 1.3806e-16 / 1.6022e-12;
      Tfact = 1.0 / (1 + sqrt(T / 1.0e5));

      
      /* Uses rates listed in Bolton & Haehnelt 2007, MNRAS, 374, 493 */
      
      /* Collisional excitation cooling erg s^-1 cm^3  (Cen, 1992, ApJS, 78, 341)*/
      if(118348 / T < 70)
	BetaH0[i] = 7.5e-19 * exp(-118348 / T) * Tfact;      

      if(13179 / T < 70)
	BetaHe0[i] = 9.1e-27 * pow(T,-0.1687) * exp(-13179 / T) * Tfact;
      
      if(473638 / T < 70)
	BetaHep[i] = 5.54e-17 * pow(T, -0.397) * exp(-473638 / T) * Tfact;
      


      /* Free-free erg s^-1 cm^3 (Cen, 1992,ApJS, 78, 341) */
      Betaff[i] = 1.43e-27 * sqrt(T) * (1.1 + 0.34 * exp(-(5.5 - log10(T)) * (5.5 - log10(T)) / 3));
      



      /* Recombination rates, cm^3s^-1 (Abel et al., 1997, NewA, 2, 181) */
      AlphaHp[i] = exp(-28.6130338 
		       - 0.72411256 * log(T_eV) 
		       - 2.02604473e-2 * pow(log(T_eV),2.0) 
		       - 2.38086188e-3 * pow(log(T_eV),3.0) 
		       - 3.21260521e-4 * pow(log(T_eV),4.0) 
		       - 1.42150291e-5 * pow(log(T_eV),5.0) 
		       + 4.98910892e-6 * pow(log(T_eV),6.0)
		       + 5.75561414e-7 * pow(log(T_eV),7.0) 
		       - 1.85676704e-8 * pow(log(T_eV),8.0)
		       - 3.07113524e-9 * pow(log(T_eV),9.0));

      AlphaHep[i] = 3.925e-13*pow(T_eV,-0.6353);
      
      if(48.596 / T_eV < 70)
	Alphad[i] = 1.544e-9 * pow(T_eV,(-1.5)) * exp(-48.596/T_eV) * (0.3+exp(8.10/T_eV));
      
      AlphaHepp[i] = 2.0 * exp(-28.6130338 
			       - 0.72411256 * log(T_eV/4.0) 
			       - 2.02604473e-2 * pow(log(T_eV/4.0),2.0)
			       - 2.38086188e-3 * pow(log(T_eV/4.0),3.0) 
			       - 3.21260521e-4 * pow(log(T_eV/4.0),4.0) 
			       - 1.42150291e-5 * pow(log(T_eV/4.0),5.0) 
			       + 4.98910892e-6 * pow(log(T_eV/4.0),6.0) 
			       + 5.75561414e-7 * pow(log(T_eV/4.0),7.0) 
			       - 1.85676704e-8 * pow(log(T_eV/4.0),8.0) 
			       - 3.07113524e-9 * pow(log(T_eV/4.0),9.0));
      



      /* Collisional ionisation rates, cm^3s^-1 (Theuns et al., 1998, MNRAS, 301, 478)*/
      if(157809.1 / T < 70)
	GammaeH0[i] = 1.17e-10 * sqrt(T) * exp(-157809.1 / T) * Tfact;
      
      if(285335.4 / T < 70)
	GammaeHe0[i] = 4.76e-11 * sqrt(T) * exp(-285335.4 / T) * Tfact;
      
      if(631515.0 / T < 70)
	GammaeHep[i] = 1.14e-11 * sqrt(T) * exp(-631515.0 / T) * Tfact;
    }
}
/******************************************************************************/
/* table input (from file TREECOOL) for ionizing parameters */
#define TABLESIZE 400		/* Max # of lines in TREECOOL */

static float inlogz[TABLESIZE];
static float gH0[TABLESIZE], gHe[TABLESIZE], gHep[TABLESIZE];
static float eH0[TABLESIZE], eHe[TABLESIZE], eHep[TABLESIZE];
static int nheattab;		/* length of table */


void ReadIonizeParams(char *fname)
{
  int i;
  FILE *fdcool;

  if(!(fdcool = fopen(fname, "r")))
    {
      printf(" Cannot read ionization table in file `%s'\n", fname);
      endrun(456);
    }

  for(i = 0; i < TABLESIZE; i++)
    gH0[i] = 0;

  for(i = 0; i < TABLESIZE; i++)
    if(fscanf(fdcool, "%g %g %g %g %g %g %g",
	      &inlogz[i], &gH0[i], &gHe[i], &gHep[i], &eH0[i], &eHe[i], &eHep[i]) == EOF)
      break;

  fclose(fdcool);

  /*  nheattab is the number of entries in the table */

  for(i = 0, nheattab = 0; i < TABLESIZE; i++)
    if(gH0[i] != 0.0)
      nheattab++;
    else
      break;

  if(ThisTask == 0)
    printf("\n\nread ionization table with %d entries in file `%s'.\n\n", nheattab, fname);
}
/******************************************************************************/
void IonizeParams(void)
{
  IonizeParamsTable();
}
/******************************************************************************/
void IonizeParamsTable(void)
{
  int i, ilow;
  double logz, dzlow, dzhi;
  double redshift;

  if(All.ComovingIntegrationOn)
    redshift = 1 / All.Time - 1;
  else
    {
      gJHe0 = gJHep = gJH0 = 0;
      epsHe0 = epsHep = epsH0 = 0;
      J_UV = 0;
      return;
    }

  logz = log10(redshift + 1.0);
  ilow = 0;
  for(i = 0; i < nheattab; i++)
    {
      if(inlogz[i] < logz)
	ilow = i;
      else
	break;
    }

  dzlow = logz - inlogz[ilow];
  dzhi = inlogz[ilow + 1] - logz;

  if(logz > inlogz[nheattab - 1] || gH0[ilow] == 0 || gH0[ilow + 1] == 0 || nheattab == 0)
    {
      gJHe0 = gJHep = gJH0 = 0;
      epsHe0 = epsHep = epsH0 = 0;
      J_UV = 0;
      return;
    }
  else
    J_UV = 1.0e-21;		/* irrelevant as long as it's not 0 */

  gJH0 = pow(10., (dzhi * log10(gH0[ilow]) + dzlow * log10(gH0[ilow + 1])) / (dzlow + dzhi));
  gJHe0 = pow(10., (dzhi * log10(gHe[ilow]) + dzlow * log10(gHe[ilow + 1])) / (dzlow + dzhi));
  gJHep = pow(10., (dzhi * log10(gHep[ilow]) + dzlow * log10(gHep[ilow + 1])) / (dzlow + dzhi));
  epsH0 = AMPL * pow(10., (dzhi * log10(eH0[ilow]) + dzlow * log10(eH0[ilow + 1])) / (dzlow + dzhi));
  epsHe0 = AMPL * pow(10., (dzhi * log10(eHe[ilow]) + dzlow * log10(eHe[ilow + 1])) / (dzlow + dzhi));
  epsHep = AMPL * pow(10., (dzhi * log10(eHep[ilow]) + dzlow * log10(eHep[ilow + 1])) / (dzlow + dzhi));

  return;
}
/******************************************************************************/
void SetZeroIonization(void)
{
  gJHe0 = gJHep = gJH0 = 0;
  epsHe0 = epsHep = epsH0 = 0;
  J_UV = 0;
}
/******************************************************************************/
void InitCool(void)
{
  InitCoolMemory();
  MakeCoolingTable();

#ifdef SFR_METALS
  read_coolrate_table();
  
#ifdef SFR_SNII
  read_yield_table();

  imf();
#endif

#endif
  
  ReadIonizeParams("TREECOOL_bestfit_g1.3");

  All.Time = All.TimeBegin;
  IonizeParams();

#ifdef LT_METAL_COOLING
  read_metalcool_table();
#endif
}
/******************************************************************************/
/* The following two routines are not used in the NE implementation */
#ifdef LT_METAL_COOLING
/*  M E T A L   C O O L I N G  */
double GetMetalLambda(double logT, double Z)
{
  double Zsol;
  double t, u, log_temp = logT;
  double x_1y, x_1y_1, xy_1;
  int Zi, Ti;

  Zsol = get_metallicity_solarunits(Z);

  getindex(&Zvalue[0], 0, 7, &Zsol, &Zi);
  if(Zi == 7)
    t = 0;
  else
    t = (Zsol - Zvalue[Zi]) / (Zvalue[Zi + 1] - Zvalue[Zi]);

  getindex(&ZTemp[0], 0, Zlength - 1, &log_temp, &Ti);
  if(Ti == Zlength - 1)
    u = 0;
  else
    u = (log_temp - ZTemp[Ti]) / (ZTemp[Ti + 1] - ZTemp[Ti]);

  x_1y = Lsuthdop[Zi + 1][Ti];
  xy_1 = Lsuthdop[Zi][Ti + 1];
  x_1y_1 = Lsuthdop[Zi + 1][Ti + 1];

  if(t == 0)
    {
      x_1y = 0;
      if(u == 0)
	x_1y_1 = 0;
    }
  if(u == 0)
    xy_1 = 0;

  return pow(10, (1 - t) * (1 - u) * Lsuthdop[Zi][Ti] +
	     t * (1 - u) * x_1y + t * u * x_1y_1 + (1 - t) * u * xy_1);
}
#endif

/******************************************************************************/
#ifdef SFR_METALS
double CoolingRate_SD(double logT, double rho, double *nelec)
{
  double Lambda, Heat;
  double LambdaCmptn = 0.0;
  double redshift;
  double T;

  if(logT <= Tmin)
    logT = Tmin + 0.5 * deltaT;	/* floor at Tmin */

  nHcgs = XH * rho / PROTONMASS;	/* hydrogen number dens in cgs units */

  if(logT < Tmax)
    {

      find_abundances_and_rates(logT, rho, nelec);

      T = pow(10.0, logT);

      /* METALS: the second input parameter is the abundance Fe/H
       */

      Lambda = get_Lambda_SD(logT, FeHgas);


      if(All.ComovingIntegrationOn)
	{
	  redshift = 1 / All.Time - 1;
	  LambdaCmptn = 5.65e-36 * ne * (T - 2.73 * (1. + redshift)) * pow(1. + redshift, 4.) / nHcgs;

	  Lambda += LambdaCmptn;
	}
      else
	LambdaCmptn = 0;

      Heat = 0;
      if(J_UV != 0)
	Heat += (nH0 * epsH0 + nHe0 * epsHe0 + nHep * epsHep) / nHcgs;
    }
  else				/* here we're outside of tabulated rates, T>Tmax K */
    {
      /* at high T (fully ionized); only free-free and Compton cooling are present.
         Assumes no heating. */

      Heat = 0;

      /* very hot: H and He both fully ionized */
      nHp = 1.0;
      nHep = 0;
      nHepp = yhelium;
      ne = nHp + 2.0 * nHepp;
      *nelec = ne;		/* note: in units of the hydrogen number density */

      T = pow(10.0, logT);

      Lambda = get_Lambda_SD(logT, FeHgas);

      if(All.ComovingIntegrationOn)
	{
	  redshift = 1 / All.Time - 1;
	  /* add inverse Compton cooling off the microwave background */
	  LambdaCmptn = 5.65e-36 * ne * (T - 2.73 * (1. + redshift)) * pow(1. + redshift, 4.) / nHcgs;
	}
      else
	LambdaCmptn = 0;

      Lambda += LambdaCmptn;
    }
  
  return (Heat - Lambda);
}
#endif
#endif
#endif /* BG_COOLING */
/******************************************************************************/
