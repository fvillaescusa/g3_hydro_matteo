import glob,os 

files = glob.glob('restart.*.bak')

for f in files:
    f_to_delete = f[:-4]               #file without .bak
    os.system('rm -r'+f_to_delete)     #delete file
    os.system('mv '+f+' '+f_to_delete) #rename the file
