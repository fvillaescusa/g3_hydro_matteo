#This script is used to generate the folders, N-GenIC parameter files, and 
#Gadget parameter files when many realizations are desired.
#The script will do the following things:
# -create the realization, correlation function and power spectrum folders
# -copy the Gadget_Pk.py file to the power spectrum folder (code name needed)
# -copy the N-GenIC file to the realization folder, modifying the seed value
#  (N-GenIC parameter file needed)
# -copy the Gadget parameter file to the realization folder
# -copy the CF file to the CF folder (xi_ariel.py filed needed)
# -execute the Gadget_Pk.py code to find the power spectra


import numpy as np
import os


################################## INPUT #######################################
number_of_files = 100     #number of realizations wanted

gadget_code = 'g3_yb_hz'  #name of code used: 'g3_yb_hz' or 'g3_hydro_matteo'

N_GenIC_file  = 'N-GenIC_0.3.param' #name of the fiducial N-GenIC file
fiducial_seed = '181170'            #seed value in the fiducial N-GenIC file

CF_file = 'xi_ariel.py'   #code for the 2pt-CF. Set to 'None' if not wanted

compute_Pk = False   #whether to execute the code Gadget_Pk.py to find P(k)
################################################################################


#do a loop over all realizations. The folder of first realization is 1, not 0
for i in xrange(1,number_of_files+1):

    #realization folder name and seed value
    folder = str(i);      seed = i*100

    #names of the power spectrum and correlation function folders
    xi_folder = folder+'/xi';       Pk_folder = folder+'/Pk_Gadget'
    
    #create folders, in case they dont exist
    if not os.access(folder,    os.F_OK):  os.makedirs(folder)    
    if not os.access(xi_folder, os.F_OK):  os.makedirs(xi_folder)
    if not os.access(Pk_folder, os.F_OK):  os.makedirs(Pk_folder)

    #copy the Gadget_Pk file to the power spectrum folder, if it is not there
    f_Pk = gadget_code+'/useful_things/Gadget_Pk.py'
    Gadget_file = Pk_folder+'/Gadget_Pk.py'
    if not os.access(Gadget_file, os.F_OK):  os.system('cp '+f_Pk+' '+Pk_folder)

    #move the N-GenIC file to the realization folder changing the seed number
    f_in  = open(N_GenIC_file,'r')
    f_out = open(folder+'/'+N_GenIC_file,'w')

    for line in f_in:
        f_out.write(line.replace(fiducial_seed,str(seed)))
    f_in.close(); f_out.close()

    #copy the Gadget parameter files and into folders
    os.system('cp ics.param '+folder)

    #copy the CF code to the CF folder, if it is not there
    if CF_file!='None':
        CF_file_realization = xi_folder+'/'+CF_file
        if not os.access(CF_file_realization, os.F_OK):
            os.system('cp '+CF_file+' '+xi_folder)

    #run the Gadget_Pk file
    if compute_Pk:
        os.system('cd '+Pk_folder+' && python Gadget_Pk.py && cd ../')


    ###################### aditional features ##########################
    #submit IC scripts
    #os.system('cd '+folder+' && bsub < script_IC && cd ..')

    #submit G3 scripts
    #os.system('cd '+folder+' && bsub < script_G3 && cd ..')

    #create Pk_Gadget folder
    #if not os.access(folder+'/Pk_Gadget', os.F_OK):
    #    os.makedirs(folder+'/Pk_Gadget')    
    #os.system('cp ratio.sh Gadget_Pk.py '+folder+'/Pk_Gadget')
        
    #execute Pk script
    #os.system('cd '+folder+'/Pk_Gadget && ./ratio.sh && cd ../..')
    ####################################################################
