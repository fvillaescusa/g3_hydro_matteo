import numpy as np
import glob
import sys


############################ INPUT #############################
root_files_m = 'Pk_matter_z='
root_f_out_m = 'ratio_matter_z='

root_files_m_CAMB = '../../CAMB_TABLES/ics_matterpow_'
root_f_out_m_CAMB = 'ratio_matter_CAMB_z='
################################################################


#get the Gadget and CAMB power spectrum files
files_m      = glob.glob(root_files_m+'*.dat')
files_m_CAMB = glob.glob(root_files_m_CAMB+'*.dat')


#obtain the redshifts of the CAMB files
start = len(root_files_m_CAMB);   end = -4  #.dat=-4 characters
z_CAMB = []
for f in files_m_CAMB:
    z_CAMB.append(float(f[start:end]))
z_CAMB = np.array(z_CAMB)


#set reference redshift and power spectrum file
start = len(root_files_m);     end = -4  #.dat=-4 characters
ref_file = files_m[0];     z_ref = float(ref_file[start:end])
index = np.where(z_CAMB==z_ref)[0]
ref_CAMB_file = files_m_CAMB[index]
print '\nreference redshift = ',z_ref
print 'reference Gadget P(k) file =',ref_file
print 'reference CAMB   P(k) file =',ref_CAMB_file,'\n'



k0,Pk0   = np.loadtxt(ref_file,unpack=True)
k0c,Pk0c = np.loadtxt(ref_CAMB_file,unpack=True)
for f in files_m[1:]:

    z = float(f[start:end])
    k1,Pk1 = np.loadtxt(f,unpack=True)
    if np.any(k0!=k1): 
        print 'k-values are different!!!'; sys.exit()
    np.savetxt(root_f_out_m+str(z)+'.dat',np.transpose([k0,Pk1/Pk0]))

    index = np.where(z_CAMB==z)[0]
    if len(index)>0:
        k1c,Pk1c = np.loadtxt(files_m_CAMB[index],unpack=True)
        if np.any(k0c!=k1c): 
            print 'CAMB k-values are different!!!'; sys.exit()
        np.savetxt(root_f_out_m_CAMB+str(z)+'.dat',
                   np.transpose([k0c,Pk1c/Pk0c]))




